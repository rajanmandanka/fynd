from rest_framework import permissions


class MovieRelatedPermission(permissions.BasePermission):
    """
    Required for Movie Model.
    Create, Update and Delete is only permitted by admin
    while list are permitted by any user who logged in.
    """

    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        elif view.action in ['list', 'retrieve']:
            return request.user.is_authenticated
        elif view.action in ['create', 'update',
                             'partial_update', 'destroy']:
            return request.user.is_superuser
        else:
            return False
