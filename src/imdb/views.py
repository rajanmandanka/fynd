from rest_framework import viewsets
from rest_framework import filters
from .serializers import MovieSerializer
from .models import Movies
from .filters import MovieFilter
from .permissions import MovieRelatedPermission


class MovieViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be view movie list.
    """
    queryset = Movies.objects.filter(is_deleted=False)
    serializer_class = MovieSerializer
    filter_backends = (filters.SearchFilter,)
    permission_classes = (MovieRelatedPermission,)
    filter_class = MovieFilter
    search_fields = ('name', 'imdb_score', 'director', 'popularity', 'genre__name')

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Movies.objects.filter(is_deleted=False)
        else:
            return None
