from django_filters import rest_framework as filters
from .models import Movies


class MovieFilter(filters.FilterSet):
    name = filters.CharFilter(
        name='name', lookup_expr='icontains')


    class Meta:
        model = Movies
        fields = {
            'imdb_score': ('gte', 'lte',),
            'popularity': ('gte', 'lte',),
        }