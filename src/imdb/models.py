from django.db import models


# Create your models here.

class Genre(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Movies(models.Model):
    name = models.CharField(max_length=255)
    imdb_score = models.FloatField(default=0.0)
    director = models.CharField(max_length=255)
    popularity = models.FloatField(default=0.0)
    genre = models.ManyToManyField(Genre)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

