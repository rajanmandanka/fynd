from rest_framework import routers
from .views import MovieViewSet

# from . import views

router = routers.DefaultRouter()
router.register(r'movies', MovieViewSet)

urlpatterns = []
urlpatterns += router.urls

