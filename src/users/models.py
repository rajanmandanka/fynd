from django.db import models
from django.contrib.auth.models import AbstractUser

# Create user related models here.
USER_TYPE = (
    ("admin", "Admin"),
    ("user", "APP User")
)


class User(AbstractUser):
    user_type = models.CharField(choices=USER_TYPE, max_length=32, default="user")