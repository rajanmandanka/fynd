from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializers import UserSerializer
from .models import User
from .utils import authenticate_user, logout_user


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be view movie list.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['post'], detail=False)
    def login(self, request, pk=None):
        """
        Authenticate a user, create a CSRF token for them,
        and return the user object as JSON.
        Note:
        You can Login after email verification is done
        """
        username = request.data.get('username')
        password = request.data.get('password')
        if not (username and password):
            content = {'detail': 'Please provide required details.'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
        else:
            user, token = authenticate_user(
                self, request, username, password)
            user_serializer = UserSerializer(user)
            serializer_data = user_serializer.data
            serializer_data['token'] = token
            return Response(serializer_data)

    @action(methods=['post'], detail=True)
    def logout(self, request, pk=None):
        """
        Logout based on the token in the header for logged in user.
        """
        logout_user(self, request)
        content = {
            'detail': 'You has been successfully logged out.'}
        return Response(content, status=status.HTTP_200_OK)
