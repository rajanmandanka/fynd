from django.contrib import admin
from .models import User
from django import forms


class UserCreationForm(forms.ModelForm):
    password = forms.CharField(
        label='Password',
        required=True, widget=forms.PasswordInput(
            render_value=True,
            attrs={'autocomplete': 'off'}
        )
    )

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user_type = self.cleaned_data.get('user_type')

        if self.cleaned_data.get("password") and not self.cleaned_data.get(
                "password"
        ).startswith('pbkdf2_sha256'):
            user.set_password(self.cleaned_data["password"])
            print("Not set")
        user.user_type = user_type
        user.save()
        return user


# Register your models here.
class UserAdmin(admin.ModelAdmin):
    fields = ('username', 'password', 'email', 'user_type')
    form = UserCreationForm

    def get_form(self, request, obj=None, **kwargs):
        form = super(UserAdmin, self).get_form(request, obj, **kwargs)
        form.request = request
        return form


admin.site.register(User, UserAdmin)
