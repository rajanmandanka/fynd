from rest_framework import serializers
from rest_framework.exceptions import ParseError
from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'user_type')
        # fields = '__all__'
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        username = validated_data.get('username', None)
        password = validated_data.get('password', None)
        user_type = validated_data.get('user_type', None)
        if username and password:
            user = User(
                username=username
            )
            user.set_password(password)
            user.user_type = user_type
            user.save()
            return user
        else:
            raise ParseError("Insufficient argument")
