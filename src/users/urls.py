from rest_framework import routers
from .views import UserViewSet

# from . import views

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)

urlpatterns = []
urlpatterns += router.urls

