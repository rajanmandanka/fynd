from django.contrib.auth import authenticate, login, logout
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.authtoken.models import Token
from .models import User


def authenticate_user(self, request, username, password):
    try:
        user_obj = User.objects.get(username=username)
    except User.DoesNotExist:
        raise NotFound('Invalid Username or Password')

    if user_obj.is_active:
        auth_user = authenticate(username=username, password=password)
        if auth_user:
            login(request, user_obj)
            token = update_or_create_token(self, request, user_obj)
            return user_obj, token
        else:
            raise PermissionDenied('User is not authenticated')
    else:
        raise PermissionDenied('User is not active')


def logout_user(self, request):
    if request.user and request.user.is_authenticated:
        Token.objects.filter(user=request.user).delete()
        logout(request)


def update_or_create_token(self, request, user_obj):
    token, created = Token.objects.get_or_create(user=user_obj)
    return token.key
