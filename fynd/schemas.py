# -*- coding: utf-8 -*-
from rest_framework.schemas import SchemaGenerator
from rest_framework import exceptions
from rest_framework.permissions import AllowAny
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_swagger import renderers


class ParamsSchemaGenerator(SchemaGenerator):
    def get_link(self, path, method, view):
        link = super(ParamsSchemaGenerator, self).get_link(path, method, view)
        replace_old, fields = self.get_core_fields(view)
        if replace_old:
            link._fields = fields
        else:
            link._fields += fields
        return link

    def get_core_fields(self, view):
        if getattr(view, 'custom_route_swagger', {}):
            custom_dict = getattr(view, 'custom_route_swagger', {})
            if view.action in custom_dict:
                return True, custom_dict[view.action]
            else:
                return False, getattr(view, 'coreapi_fields', ())
        else:
            return False, getattr(view, 'coreapi_fields', ())


def get_params_swagger_view(title=None, url=None):
    """
    Returns schema view which renders Swagger/OpenAPI.

    (Replace with DRF get_schema_view shortcut in 3.5)
    """

    class SwaggerSchemaView(APIView):
        _ignore_model_permissions = True
        exclude_from_schema = True
        permission_classes = [AllowAny]
        renderer_classes = [
            CoreJSONRenderer,
            renderers.OpenAPIRenderer,
            renderers.SwaggerUIRenderer
        ]

        def get(self, request):
            generator = ParamsSchemaGenerator(title=title, url=url)
            schema = generator.get_schema(request=request)

            if not schema:
                raise exceptions.ValidationError(
                    'The schema generator did not return a schema Document'
                )

            return Response(schema)

    return SwaggerSchemaView.as_view()
